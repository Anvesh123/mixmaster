import {RouterProvider, createBrowserRouter} from 'react-router-dom';

import { Landing, Error, HomeLayout, Cocktail, About, SingleCocktail } from './pages/index';
import {loader as landingLoader } from './pages/Landing';
import {loader as SingleCocktailLoader } from './pages/SingleCocktail';
import DedicatedErrorComponent from './components/DedicatedErrorComponent';

const router = createBrowserRouter( [
{
  path: '/',
  element: <HomeLayout />,
  errorElement: <Error />,
  children: [
    {
      path: '/about',
      element: <About />
    },

    {
      path: '/cocktail',
      element: <Cocktail />
    },

    {
      path: '/landing',
      index: true, 
      loader: landingLoader,
      element: <Landing /> ,
      errorElement: <DedicatedErrorComponent />
    },

    {
      path: '/cocktail/:id',
      element: <SingleCocktail />,
      errorElement: <DedicatedErrorComponent />,
      loader: SingleCocktailLoader
    }
  ]
},


 ])

const App = () => {
  return <RouterProvider  router={router} />
};
export default App;
