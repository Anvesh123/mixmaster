import React from 'react';
import { Link, useOutletContext } from 'react-router-dom';

import Wrapper from '../assets/wrappers/CocktailCard';


export const Cocktail = ({id, drinkName, thumbnail, type}) => {
   const data  = useOutletContext();
   console.log('data :', data);
  return (
    <Wrapper>
       <div className='image-container'>
          <img src={thumbnail} alt='image' />
       </div>
       <div className='footer'>
          <h4>{drinkName}</h4>
          <h4>{type}</h4>
          <Link to={`/cocktail/${id}`} className='btn'>Details</Link>
       </div>
    </Wrapper>
  )
}

