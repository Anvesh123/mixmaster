import React from 'react';
import { useRouteError  } from 'react-router-dom';

const DedicatedErrorComponent = () => {
    const error  = useRouteError();
  return (
    <h5>{error.message}</h5>
  )
}

export default DedicatedErrorComponent;