import { Cocktail } from  './Cocktail';
import Wrapper from '../assets/wrappers/CocktailList';

const CocktailList = ({drinks}) => {
  console.log('cocktail list rendered');
    const formattedDrinks = drinks.map( (drink) => {
        const {idDrink, strDrink, strDrinkThumb, strGlass } = drink;
        return ( 
                 {
                    id: idDrink,
                    drinkName: strDrink, 
                    thumbnail: strDrinkThumb, 
                    type: strGlass 
                 }
                )
    })
  return (
    <Wrapper>
        {
            formattedDrinks.map( (drink) => {
                // console.log('drink', drink);
                
                return ( 
                         <Cocktail { ...drink } /> 
                        )
            })
        }
    </Wrapper>
  )
}

export default CocktailList;