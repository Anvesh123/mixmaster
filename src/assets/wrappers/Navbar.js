import styled from 'styled-components';

const Wrapper = styled.nav`
background: black;
.nav-center{
  width: var(--view-width);
  max-width: var(--max-width);
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding:1rem 2rem;
  background: var(--grey-300);
}

.logo { 
  color: var(--primary-500);
  font-size: 2rem;
  font-weight: 650;
}

.nav-links{
  // background: red;
  display: flex;
  gap: 1rem;
  // padding: 0 0.5rem;
}

.nav-link{
  transition: var(--transition);
  letter-spacing: 1.3px;
  :hover {
    color: var(--primary-500);
  }
}
`;






// background: var(--white);
// .nav-center {
//   width: var(--view-width);
//   max-width: var(--max-width);
//   margin: 0 auto;
//   display: flex;
//   flex-direction: column;
//   padding: 1.5rem 2rem;
// }
// .logo {
//   font-size: clamp(1.5rem, 3vw, 3rem);
//   color: var(--primary-500);
//   font-weight: 700;
//   letter-spacing: 2px;
// }
// .nav-links {
//   display: flex;
//   flex-direction: column;
//   gap: 0.5rem;
//   margin-top: 1rem;
// }
// .nav-link {
//   color: var(--grey-900);
//   padding: 0.5rem 0.5rem 0.5rem 0;
//   transition: var(--transition);
//   letter-spacing: 2px;
// }
// .nav-link:hover {
//   color: var(--primary-500);
// }
// .active {
//   color: var(--primary-500);
// }
// @media (min-width: 768px) {
//   .nav-center {
//     flex-direction: row;
//     justify-content: space-between;
//     align-items: center;
//   }
//   .nav-links {
//     flex-direction: row;
//     margin-top: 0;
//   }
// }

export default Wrapper;
