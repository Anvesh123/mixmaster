import styled from 'styled-components';

const Wrapper = styled.div`
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  img {
    width: 90vw;
    max-width: 600px;
    margin-bottom: 1rem;
  } 
  h2 {
    margin-bottom: 0.5rem;
  }
  p {
    margin-bottom: 0.5rem;
  }

  a {
    color:  var(--primary-500);
    text-transform: capitalize;
    text-decoration: underline;
  }

 `;

export default Wrapper;
