import React from 'react';
import { useLoaderData } from 'react-router-dom';
import axios from 'axios'

export const loader = async({params})=> {
    const { id } = params;
    console.log('params :', params);
    const baseURL = 'https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=';
    const  { data } = await axios.get(`${baseURL}${id}`)
    const drink  = data.drinks[0];
    return  { drink };

}
const SingleCocktail = () => {
    const { drink } = useLoaderData();  
    console.log('drink :',drink)
    const {
        strDrink: name,
        strDrinkThumb: image,
        strAlcoholic: info,
        strCategory: category,
        strGlass: glass,
        strInstructions: instructions,
      } = drink;
    return (
    <div>SingleCocktail</div>
  )
}

export default SingleCocktail;