import { useLoaderData, useRouteError } from 'react-router-dom';
import axios from 'axios';
import CocktailList from '../components/CocktailList'; 

export const loader = async()=> {
  const response = await axios.get('https://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita')
  return response.data.drinks;
}

const Landing = () => {
  const drinks = useLoaderData( );
  console.log(drinks);
  return (
    <>
      <CocktailList drinks = { drinks } > </CocktailList>
    </>
  )
}

export default Landing;