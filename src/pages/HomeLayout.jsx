import React from 'react';
import { Outlet, useNavigation, useOutletContext } from 'react-router-dom';

import Navbar from  '../components/Navbar';

const HomeLayout = () => {
  const navigation = useNavigation();
  console.log('navigation :', navigation);
  const value = 'some value';
  return (
    <div>
        <Navbar></Navbar>
        <section className='page'> 
          { navigation.status === 'loading'  ? <div> Loading...</div> : <Outlet context = { {value} } /> }
        </section>
    </div>
  )
}

export default HomeLayout;