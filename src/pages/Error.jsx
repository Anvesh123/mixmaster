import React from 'react';
import Wrapper from '../assets/wrappers/ErrorPage';
import {Link, useRouteError} from 'react-router-dom';
import ErrorPic_404  from '../assets/not-found.svg';

const Error = () => {
  const error = useRouteError();
  if(error.status = 404 ) 
    {
      return (
        <Wrapper>
          <div>
            <img src={ ErrorPic_404 } alt="" />
            <h2> Oops! </h2>
            <p>we cant find the page u are looking for</p>
            <Link to='/landing'>Back to home</Link>
          </div>
        </Wrapper>
      )
    }
  return (
    <Wrapper>
      <div>
        <h2>Some thing went wrong :error code - `${error.status}`</h2>
      </div>
    </Wrapper>
  )  

}

export default Error;