import React from 'react';
import { useOutletContext } from 'react-router-dom';
import Wrapper from '../assets/wrappers/AboutPage';

const About = () => {
 
  return (
    <Wrapper>
      <h3>About us</h3>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint repudiandae
         deleniti ullam libero molestiae commodi amet? Et, quam. Eos quidem, at 
         tempore corporis sequi vero voluptate est tenetur dolores eveniet dicta 
         blanditiis atque perspiciatis numquam veniam omnis harum sed molestiae!
      </p>
    </Wrapper>
  )
}

export default About;